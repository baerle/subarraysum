<?php


class Stopwatch
{
    private int $start;

    /**
     * Stopwatch constructor.
     */
    public function __construct()
    {
        $this->start = hrtime(true);
    }

    /**
     * @return int
     */
    public function elapsedNanos(): int
    {
        return hrtime(true) - $this->start;
    }

    /**
     * @param int $nanos
     * @return float
     */
    public function nanosInSeconds(int $nanos): float
    {
        return $nanos / 1_000_000_000.0;
    }

    /**
     * @return float
     */
    public function elapsedSeconds(): float
    {
        return $this->nanosInSeconds($this->elapsedNanos());
    }
}