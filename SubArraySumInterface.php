<?php


interface SubArraySumInterface
{
    public function getSubSum(int $from, int $until): int;
}