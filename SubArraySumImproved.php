<?php


class SubArraySumImproved implements SubArraySumInterface
{
    private array $summedUp;

    /**
     * SubArraySumImproved constructor.
     * @param array $array
     */
    public function __construct(array $array)
    {
        $sum = 0;
        for ($i = 0; $i < sizeof($array); $i++) {
            $sum += $array[$i];
            $this->summedUp[$i] = $sum;
        }
    }

    /**
     * @param int $from
     * @param int $until
     * @return int
     */
    public function getSubSum(int $from, int $until): int
    {
        if ($from != 0)
            return $this->summedUp[$until] - $this->summedUp[$from - 1];
        return $this->summedUp[$until];
    }
}