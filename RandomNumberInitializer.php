<?php


class RandomNumberInitializer
{
    private string $filename;

    /**
     * RandomNumberInitializer constructor.
     * @param string $filename
     */
    public function __construct(string $filename)
    {
        $this->filename = $filename;
    }


    /**
     * @param int $length
     * @param array $randoms
     * @param array $array
     */
    function initializeRandomNumbers(int $length, array &$randoms, array &$array)
    {
        $count = 0;
        $content = file_get_contents($this->filename);
        $lines = explode(PHP_EOL, $content);
        foreach ($lines as $line) {
            $number = explode(',', $line);
            if (count($number) !== 1) {
                $randoms[] = [abs($number[0]) % $length, abs($number[1]) % $length];
                if ($count < $length) {
                    $array[] = $number[0];
                    $count++;
                }
            }
        }
    }
}