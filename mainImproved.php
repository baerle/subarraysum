<?php

require 'Stopwatch.php';
require 'SubArraySumInterface.php';
require 'SubArraySumImproved.php';
require 'RandomNumberInitializer.php';

opcache_reset();

$filename = './randoms.txt';

$length = 10;
$array = [];
$randoms = [];

$randInitializer = new RandomNumberInitializer($filename);
$randInitializer->initializeRandomNumbers($length, $randoms, $array);


$improved = new Stopwatch();
$subArraySumImproved = new SubArraySumImproved($array);
foreach ($randoms as $randomArray) {
    $stopwatch = new Stopwatch();
    $result = $subArraySumImproved->getSubSum($randomArray[0], $randomArray[1]);
    /*printSubSum($randomArray[0], $randomArray[1], $result);
    echo printf("%10f", $stopwatch->elapsedSeconds()) . PHP_EOL;*/
}
$improvedTime = $improved->elapsedSeconds();
echo "complete improved:" . $improved->elapsedSeconds();


/**
 * @param int $random1
 * @param int $random2
 * @param int $result
 */
function printSubSum(int $random1, int $random2, int $result)
{
    echo "[" . $random1 . ":" . $random2 . "]: " . $result . PHP_EOL;
}

