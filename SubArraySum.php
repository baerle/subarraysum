<?php


class SubArraySum implements SubArraySumInterface
{
    private array $array;

    /**
     * SubArraySum constructor.
     * @param array $array
     */
    public function __construct(array $array)
    {
        $this->array = $array;
    }

    /**
     * @param int $from
     * @param int $until
     * @return int
     */
    public function getSubSum(int $from, int $until): int
    {
        $sum = 0;
        for ($i = $from; $i <= $until; $i++) {
            $sum += $this->array[$i];
        }
        return $sum;
    }
}